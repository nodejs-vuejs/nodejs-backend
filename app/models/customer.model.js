const mongoose = require('mongoose');

const CustomerSchema = mongoose.Schema({
    name: String,
    age: {type: Number, min: 18, max: 65, required: true},
    active: {
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model('Customer', CustomerSchema);